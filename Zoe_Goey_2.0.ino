const int REYE = A3;
const int LEYE = A4;

const int LPOS = 4;
const int LNEG = 5; // Digital pins 5 and 6 can deliver analog values as
const int RNEG = 6; // well as digital values so are used to power motors
const int RPOS = 7;

const int US_ECHO = 8;
const int US_TRIG = 9;

const int SLOW = 200; // Power delivered to the motors during Analog functions between 0 and 255. (Seems to only have enough power to move at aaround 125)

const int SDIST = 12;// Distance in cm obstical has to be in frount of ultrasonic sensors for buggy to stop

//const int PanID = 3107;

int distance;
long duration;
int count = 0; //Used to keep track of how many loops have been compleated

void forward();
void backward();
void stopMotors();
void turnRight();
void turnLeft();

void Analog_forward();
void Analog_backward();
void Analog_turnRight();
void Analog_turnLeft();

void ultrasonicPulse();

void setup() {
  Serial.begin(9600);
  delay(  1000  );  //  Guard
  Serial.print("+++");
  delay(  1000  );
  Serial.println("ATID  3107, CH  C,  CN");
  delay(  1000  );
  while (  Serial.read() !=  -1  );

  pinMode(LEYE, INPUT );
  pinMode(REYE, INPUT );
  pinMode(LNEG, OUTPUT);
  pinMode(LPOS, OUTPUT);
  pinMode(RPOS, OUTPUT);
  pinMode(RNEG, OUTPUT);

  pinMode(US_ECHO, INPUT);
  pinMode(US_TRIG, OUTPUT);
  /*
    Analog_forward();
    delay(10000);
    backward();
    delay(3000);
    turnRight();
    delay(1000);
    turnLeft();
    delay(1000);
    stopMotors();
    delay(1000);
  */
}

void loop() {
  bool Seeing_Black_Left  = digitalRead(LEYE);
  bool Seeing_Black_Right = digitalRead(REYE);
  char theChar; //char being read from XBee

  if ( Serial.available()  > 0 ) {
    theChar = Serial.read();
    if (theChar == 's') {
      stopMotors();
    }
    if (theChar == 'g') {
      if ( Seeing_Black_Left  &&  Seeing_Black_Right ) {
        //Serial.println("Seeing black both sides so going");
        Analog_forward();
      }
      if ( !Seeing_Black_Left && !Seeing_Black_Right ) {
        //Serial.println("Seeing white both sides so going");
        Analog_forward();
      }
      if ( !Seeing_Black_Left && Seeing_Black_Right ) {
        //Serial.println("Seeing white on left and black on right");
        Analog_turnLeft();
      }
      if ( Seeing_Black_Left && !Seeing_Black_Right ) {
        //Serial.println("Seeing black on left and white on right");
        Analog_turnRight();
      }

      if ( (count % 10) == 0 ) { // ie only does this every 10 loops
        ultrasonicPulse();

        while (distance <= SDIST) {
          stopMotors();

          delay(500);

          ultrasonicPulse();
        }
      }
      count++;
    }

  }

}


void forward() {
  digitalWrite(LNEG, HIGH);  //NEG = HIGH and POS = LOW to go forward
  digitalWrite(LPOS, LOW);
  digitalWrite(RPOS, LOW);
  digitalWrite(RNEG, HIGH);
}

void backward() {
  digitalWrite(LNEG, LOW);   //POS = HIGH and NEG = LOW to go backward
  digitalWrite(LPOS, HIGH);
  digitalWrite(RPOS, HIGH);
  digitalWrite(RNEG, LOW);
}

void stopMotors() {
  digitalWrite(LNEG, LOW);
  digitalWrite(LPOS, LOW);
  digitalWrite(RPOS, LOW);
  digitalWrite(RNEG, LOW);
}

void turnRight() {
  digitalWrite(LNEG, LOW);
  digitalWrite(LPOS, HIGH);
  digitalWrite(RPOS, LOW);
  digitalWrite(RNEG, HIGH);
}

void turnLeft() {
  digitalWrite(LNEG, HIGH);
  digitalWrite(LPOS, LOW);
  digitalWrite(RPOS, HIGH);
  digitalWrite(RNEG, LOW);
}








void Analog_forward() {
  analogWrite(LNEG, SLOW);  //Speed of analog functions can be changed by changing SLOW variable at top of function
  analogWrite(LPOS, LOW);
  analogWrite(RPOS, LOW);
  analogWrite(RNEG, SLOW);
}

void Analog_backward() {
  analogWrite(LNEG, LOW);
  analogWrite(LPOS, SLOW);
  analogWrite(RPOS, SLOW);
  analogWrite(RNEG, LOW);
}

void Analog_turnRight() {
  analogWrite(LNEG, LOW);
  analogWrite(LPOS, LOW);
  analogWrite(RPOS, LOW);
  analogWrite(RNEG, SLOW);
}

void Analog_turnLeft() {
  analogWrite(LNEG, SLOW);
  analogWrite(LPOS, LOW);
  analogWrite(RPOS, LOW);
  analogWrite(RNEG, LOW);
}


void ultrasonicPulse() {
  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);

  digitalWrite( US_TRIG, HIGH );  //NOT SURE THIS IS RIGHT CHECK
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );
  duration = pulseIn( US_ECHO, HIGH );
  distance = duration / 58;
}
